#!/bin/bash

# Make backups

# Copy configs
sudo cp ../etc/fail2ban/jail.local /etc/fail2ban/jail.local
sudo cp ../etc/fail2ban/paths-overrides.local /etc/fail2ban/paths-overrides.local
sudo cp ../etc/fail2ban/firewalld.local /etc/fail2ban/jail.d/00-firewalld.local
sudo cp ../etc/fail2ban/sshd.local /etc/fail2ban/jail.d/00-sshd.local



# Restart services
sudo /etc/init.d/fail2ban restart
sudo /etc/init.d/sshd restart
